﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Assessment2
{
    public partial class Options : ContentPage
    {
        public Options()
        {
            InitializeComponent();

            var listShow = new List<myListView>
            {
                new myListView {topic = "Faculty", info="Contact the people in charge", image="teachers.jpg" },
                new myListView {topic="Courses", info="Decide which career you'll follow", image="desk.jpeg" },
                new myListView { topic = "Language", info="Change language settings", image="world.png" },
                new myListView { topic = "Your Grades", info="See what you got in current/previous courses", image="Aplus.jpg" }

            };
                        
            myListShow.ItemsSource = listShow;
        }

        async void BackToMainPage(object sender, System.EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

    }
}

