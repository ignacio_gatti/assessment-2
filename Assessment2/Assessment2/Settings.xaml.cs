﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Assessment2
{
    public partial class Settings : ContentPage
    {
        
        public Settings()
        {
            InitializeComponent();

            
        }

        async void BackToMainPage(object sender, System.EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        

        void LogIn(object sender, EventArgs e)
        {
            Random rnd = new Random();
            int day = rnd.Next(1, 32);
            int month = rnd.Next(1, 13);
            int year = rnd.Next(1960, 2000);
            
            if(login.Opacity == 0)
            {
                //DO NOTHING
            }

            else if (login.Opacity == 1)
            {
                if (fullnameEntry.Text == "" || studentIdEntry.Text == "" || PasswordEntry.Text == "")
                {

                    DisplayAlert("Invalid!", "You Need to Fill All Areas", "Ok");
                }

                else
                {
                    login.Opacity = 0;
                    logout.Opacity = 1;
                    Row1.Height = new GridLength(0, GridUnitType.Star);
                    Row2.Height = new GridLength(2, GridUnitType.Star);
                    nameOutput.Text = $"Name: {fullnameEntry.Text}";
                    studentIdOutput.Text = $"Student ID: {studentIdEntry.Text}";
                    dobOutput.Text = $"Date of Birth {day}/{month}/{year}";
                }
            }

            



        }

        void LogOut(object sender, EventArgs e)
        {
            if (logout.Opacity == 0)
            {
                //DO NOTHING
            }

            else if (logout.Opacity == 1)
            {
                login.Opacity = 1;
                logout.Opacity = 0;
                fullnameEntry.Text = "";
                studentIdEntry.Text = "";
                PasswordEntry.Text = "";
                Row1.Height = new GridLength(2, GridUnitType.Star);
                Row2.Height = new GridLength(0, GridUnitType.Star);
            }

                
            
        }
    }
}
