﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Assessment2
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            var imagetap1 = new TapGestureRecognizer();
            var imagetap2 = new TapGestureRecognizer();
            var imagetap3 = new TapGestureRecognizer();
            imagetap1.Tapped += facebooktap;
            imagetap2.Tapped += twittertap;
            imagetap3.Tapped += moodletap;


            facebookicon.GestureRecognizers.Add(imagetap1);
            twittericon.GestureRecognizers.Add(imagetap2);
            moodleicon.GestureRecognizers.Add(imagetap3);
        }

        async void GoOptions(object sender, System.EventArgs e)
        {
            await Navigation.PushModalAsync(new Options());
        }

        async void GoSettings(object sender, System.EventArgs e)
        {
            await Navigation.PushModalAsync(new Settings());
        }

        async void showBusRoute(object sender, System.EventArgs e)
        {
            await Navigation.PushModalAsync(new BusRoute());
        }

        async void facebooktap (object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new FacebookWebView());
        }

        async void twittertap(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new TwitterWebView());
        }

        async void moodletap(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new MoodleWebView());
        }

    }
}
