﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Assessment2
{
    public partial class BusRoute : ContentPage
    {
        public BusRoute()
        {
            InitializeComponent();

        }

        public string BusNumbersStatus = "closed";

        async void BackToMainPage(object sender, System.EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        void BusMapSwitch(object sender, EventArgs e)
        {
            

            if(BusNumbersStatus == "closed")
            {

                Row2.Height = new GridLength(2, GridUnitType.Star);
                Row1.Height = new GridLength(0, GridUnitType.Star);
                BusNumbersStatus = "opened";
                BusMapLabel.Text = "Show Bus Map";
                
            }

            else if (BusNumbersStatus == "opened")
            {
                Row1.Height = new GridLength(2, GridUnitType.Star);
                Row2.Height = new GridLength(0, GridUnitType.Star);
                BusNumbersStatus = "closed";
                BusMapLabel.Text = "Show Bus Numbers";
            }
            
        }



    }
}
